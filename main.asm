global _start
extern print_string
extern print_error
extern find_word
extern read_word
extern string_length
extern exit


section .data
err_msg: db "Not Found", 0
%include "words.inc"


section .text
_start:
    sub rsp, 255
    mov rdi, rsp
    mov rsi, 255
    call read_word
    mov rdi, rax
    mov rsi, next    
    call find_word
    test rax, rax
    jz .err
    mov rdi, rax
    add rsp, 255
    call print_string
    call exit

.err:
    mov rdi, err_msg
    call print_error
    call exit

