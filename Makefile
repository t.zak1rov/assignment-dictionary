ASM=nasm
AFLAGS=-felf64


main:	lib.o dict.o main.o
	ld -o main lib.o dict.o main.o

lib.o:	lib.asm
	$(ASM) $(AFLAGS) -o  lib.o lib.asm

dict.o:	dict.asm
	$(ASM) $(AFLAGS) -o  dict.o dict.asm

main.o:	main.asm
	$(ASM) $(AFLAGS) -o  main.o main.asm


clean:
	rm main.o lib.o dict.o main

