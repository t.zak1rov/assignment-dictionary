extern string_equals
extern string_length
global find_word

find_word:
    xor rax, rax
.loop:
    test rsi, rsi
    jz .end

    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .found
    mov rsi, [rsi]
    jmp .loop

.found:
    call string_length
    add rax, rsi
    add rax, 9
.end:
    ret    
    
